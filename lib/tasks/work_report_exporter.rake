# redmine-plugin-work-report-exporter
# Copyright (C) 2021-2022  ClearCode Inc.
# Copyright (C) 2022-2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WorkReportExporter
  class InitialCustomFieldsManager
    def create
      initialize_locale
      settings = Setting.plugin_work_report_exporter
      WorkReportExporter::Settings::PROJECT_ITEMS.each do |key, params|
        params = params.dup
        fill_params(key, params)
        if ProjectCustomField.where(params).empty?
          ProjectCustomField.create!(params)
          if params[:default_value]
            settings.set_default_value(key, params[:default_value])
          end
        end
        settings.set_field_name(key, params[:name])
      end
      WorkReportExporter::Settings::VERSION_ITEMS.each do |key, params|
        params = params.dup
        fill_params(key, params)
        if VersionCustomField.where(params).empty?
          VersionCustomField.create!(params)
          if params[:default_value]
            settings.set_default_value(key, params[:default_value])
          end
        end
        settings.set_field_name(key, params[:name])
      end
      Setting.plugin_work_report_exporter = settings.to_h
    end

    def destroy
      initialize_locale
      WorkReportExporter::Settings::PROJECT_ITEMS.each do |key, params|
        ProjectCustomField.where({ name: default_field_name(key) }).destroy_all
      end
      WorkReportExporter::Settings::VERSION_ITEMS.each do |key, params|
        VersionCustomField.where({ name: default_field_name(key) }).destroy_all
      end
      Setting.plugin_work_report_exporter = {}
    end

    private
    def initialize_locale
      if ENV["RAILS_LOCALE"]
        I18n.locale = ENV["RAILS_LOCALE"].to_sym
      else
        I18n.locale = :en
      end
    end

    def t(key)
      I18n.t(key)
    end

    def prefix
      ENV["PREFIX"] || ""
    end

    def max_name_size
      CustomField.validators_on(:name).find do |validator|
        validator.is_a?(ActiveRecord::Validations::LengthValidator)
      end.options[:maximum]
    end

    def default_field_name(key)
      localized_name = t("work_report_exporter.custom_field_name.#{key}")
      "#{prefix}#{localized_name}"[0...max_name_size]
    end

    def fill_params(key, params)
      params[:field_format] ||= "string"
      params[:name] = default_field_name(key)
      if params[:field_format] == "string"
        default_value_key = "work_report_exporter.default_value.#{key}"
        params[:default_value] ||= t(default_value_key)
      end
    end
  end
end

namespace :work_report_exporter do
  desc "Tag"
  task :tag => :environment do
    plugin = Redmine::Plugin.find(:work_report_exporter)
    version = plugin.version
    cd(plugin.directory) do
      sh("git", "tag",
         "-a", version,
         "-m", "#{version} has been released!!!")
      sh("git", "push", "--tags")
    end
  end

  desc "Release"
  task :release => :tag

  namespace :initial_custom_field do
    desc "Create initial custom fields"
    task :create => :environment do
      manager = WorkReportExporter::InitialCustomFieldsManager.new
      manager.create
    end

    desc "Destroy initial custom fields"
    task :destroy => :environment do
      manager = WorkReportExporter::InitialCustomFieldsManager.new
      manager.destroy
    end
  end
end
