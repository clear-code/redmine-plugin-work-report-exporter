# redmine-plugin-work-report-exporter
# Copyright (C) 2021,2022 ClearCode Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class WorkReportsController < TimelogController
  helper_method :params

  def index
    retrieve_time_entry_query
    scope = time_entry_scope.
      preload(:issue => [:project, :tracker, :status, :assigned_to, :priority]).
      preload(:project, :user)

    respond_to do |format|
      format.xlsx do
        @entries = scope.preload(:custom_values => :custom_field).to_a
        created_date = params[:created_date]
        if created_date.blank?
          created_date = User.current.today
        else
          created_date = Date.parse(created_date)
        end
        reporter = XlsxReporter::Reporter.new({
          project_id: project.id,
          customer: customer,
          contracts: contracts,
          created_date: created_date,
        })
        xlsx = Mime[:xlsx]
        send_data(reporter.report.to_stream.read,
                  :type => "#{xlsx}; header=present",
                  :filename => filename)
      end
    end
  end

  private
  def project
    @project ||= Project.find(params[:project_id])
  end

  def customer
    @customer ||= WorkReportExporter::Customer.new(@project)
  end

  def contracts
    @contracts ||= versions.collect do |version|
      contract = WorkReportExporter::Contract.new(version, @entries)
      if contract.valid?
        contract
      else
        nil
      end
    end.compact
  end

  def versions
    @version ||= prepare_versions
  end

  def prepare_versions
    filtered_version = params["issue.fixed_version_id"]
    if filtered_version.present?
      if filtered_version.is_a?(Array)
        filtered_version = filtered_version.first
      end
      return [Version.find(filtered_version)]
    end

    effective_versions = customer.effective_versions

    fixed_versions = @entries.collect do |entry|
      entry.issue and entry.issue.fixed_version
    end.uniq.compact
    return [] if fixed_versions.empty?

    fixed_versions.select do |version|
      effective_versions.include?(version)
    end.sort do |version|
      version.effective_date
    end
  end

  def filename
    "#{customer.company_name}-#{date_stamp}.xlsx"
  end

  def date_stamp
    WorkReportExporter::DateFormatter.format(date)
  end

  def date
    date_from_filter || User.current.today
  end

  def date_from_filter
    if @query.filters["spent_on"]
      date_from_spent_on
    else
      nil
    end
  end

  def date_from_spent_on
    filter = @query.filters["spent_on"]
    value = filter[:values]
    case filter[:operator]
    when "=", "<="
      # =  : equals to the date
      # <= : before the date
      if value.any? and not value.first.empty?
        Date.parse(value.first)
      end
    when ">="
      # after the date
      nil
    when "><"
      # in the range
      if value.any? and not value.first.empty?
        value.collect do |date|
          Date.parse(date)
        end.sort.last
      end
    when "><t-", ">t-", "<t-", "t-", "t", "w", "lw", "l2w", "m", "y"
      # ><t- : between today - n-days ago and today
      # >t-  : >= between today - n-days ago
      # <t-  : <= between today - n-days ago
      # t-   : n days in past
      # t    : today
      # w    : this week
      # lw   : last week
      # l2w  : last 2 weeks
      # m    : this month
      # y    : this year
      User.current.today
    when "ld"
      # = yesterday
      User.current.today.yesterday
    when "lm"
      # last month
      User.current.today.prev_month
    else
      # unsupported
      nil
    end
  end
end
