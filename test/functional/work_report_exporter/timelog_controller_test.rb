# redmine-plugin-work-report-exporter
# Copyright (C) 2022  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require File.expand_path("../../../test_helper", __FILE__)

module WorkReportExporter
  class TimelogControllerTest < Redmine::ControllerTest
    tests TimelogController

    fixtures :custom_fields
    fixtures :custom_fields_projects
    fixtures :custom_fields_trackers
    fixtures :custom_values
    fixtures :enabled_modules
    fixtures :enumerations
    fixtures :issue_categories
    fixtures :issue_statuses
    fixtures :issues
    fixtures :member_roles
    fixtures :members
    fixtures :projects
    fixtures :projects_trackers
    fixtures :roles
    fixtures :time_entries
    fixtures :trackers
    fixtures :users
    fixtures :versions

    include Redmine::I18n

    def test_index_other_formats_links
      @request.session[:user_id] = 2

      project_id = "ecookbook"
      get :index, :params => {:project_id => project_id}
      assert_response :success

      other_formats_links = css_select(".other-formats a").collect do |a|
        [a.text, a[:href]]
      end
      user = User.current
      if user.respond_to?(:atom_key)
        atom_key = user.atom_key
      else
        atom_key = user.rss_key
      end
      assert_equal([
                     [
                       "Atom",
                       project_time_entries_path(project_id: project_id,
                                                 format: "atom",
                                                 key: atom_key),
                     ],
                     [
                       "CSV",
                       project_time_entries_path(project_id: project_id,
                                                 format: "csv"),
                     ],
                     [
                       l("work_report_exporter.label.xlsx_link"),
                      project_work_reports_path(project_id: project_id,
                                                format: "xlsx"),
                     ],
                   ],
                   other_formats_links)
    end
  end
end
